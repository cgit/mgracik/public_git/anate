import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('anate.baseutils')

import os
import subprocess


class Subprocess(object):
    def __init__(self, cmd, *args):
        self._cmd = [cmd]
        self._args = map(str, list(args))

        self._rc = None
        self._stdout = []
        self._stderr = []

    def run(self):
        logger.info('running command %s', self._cmd + self._args)
        proc = subprocess.Popen(self._cmd + self._args,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        while True:
            (out_str, err_str) = proc.communicate()
            if out_str:
                self._stdout.append(out_str)
            if err_str:
                self._stderr.append(err_str)

            if proc.returncode is not None:
                self._rc = proc.returncode
                break

    @property
    def rc(self):
        return self._rc

    @property
    def stdout(self):
        return self._stdout

    @property
    def stderr(self):
        return self._stderr
