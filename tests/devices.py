import unittest
import anate.devices as devices


class DevicesTestCase(unittest.TestCase):
    def setUp(self):
        self._remove_node = False
        self.device_node = devices.BlockDeviceNode('/dev/loop0', major=7, minor=0)
        if not self.device_node.available:
            self.device_node.create()
            self._remove_node = True

    def tearDown(self):
        if self._remove_node:
            self.device_node.unlink()

    def testBlockDeviceClass(self):
        device_object = devices.BlockDevice('device0', self.device_node)
        self.assertEqual(device_object.name, 'device0')
        self.assertEqual(device_object.path, '/dev/loop0')
        self.assertEqual(device_object.active, False)
        self.assertEqual(device_object.major, 7)
        self.assertEqual(device_object.minor, 0)

        device_object.initialize()
        self.assertEqual(device_object.active, True)

        device_object.destroy()
        self.assertEqual(device_object.active, False)

    def testStorageDeviceClass(self):
        storage_object = devices.StorageDevice('storage0', self.device_node, size=20)
        self.assertEqual(storage_object.name, 'storage0')
        self.assertEqual(storage_object.path, '/dev/loop0')
        self.assertEqual(storage_object.size, 20)
        self.assertEqual(storage_object.active, False)
        self.assertEqual(storage_object.major, 7)
        self.assertEqual(storage_object.minor, 0)

        storage_object.initialize()
        self.assertEqual(storage_object.active, True)

        storage_object.destroy()
        self.assertEqual(storage_object.active, False)

    def testLoopDeviceClass(self):
        loop_object = devices.LoopDevice('loop0', self.device_node, '/tmp/file0', size=20)
        self.assertEqual(loop_object.name, 'loop0')
        self.assertEqual(loop_object.path, '/dev/loop0')
        self.assertEqual(loop_object.active, False)
        self.assertEqual(loop_object.major, 7)
        self.assertEqual(loop_object.minor, 0)

        loop_object.initialize()
        self.assertEqual(loop_object.active, True)

        loop_object.destroy()
        self.assertEqual(loop_object.active, False)

    def testDMDeviceClass(self):
        loop_object = devices.LoopDevice('loop0', self.device_node, '/tmp/file0', size=20)
        loop_object.initialize()

        dm_object = devices.DMDevice('mydm0', loop_object)
        self.assertEqual(dm_object.name, 'mydm0')
        self.assertEqual(dm_object.path, '/dev/mapper/mydm0')
        self.assertEqual(dm_object.size, loop_object.size)
        self.assertEqual(dm_object.usable_size, loop_object.size)
        self.assertEqual(dm_object.active, False)

        dm_object.initialize()
        self.assertEqual(dm_object.active, True)

        major, minor = devices.getDeviceNumbers(dm_object.path)
        self.assertEqual(dm_object.major, major)
        self.assertEqual(dm_object.minor, minor)

        dm_object.destroy()
        self.assertEqual(dm_object.active, False)

        dm_object = devices.DMDevice('mydm0', loop_object, size=40)
        self.assertEqual(dm_object.size, 40)
        self.assertEqual(dm_object.usable_size, loop_object.size)

        dm_object.initialize()
        dm_object.destroy()

        loop_object.destroy()


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(DevicesTestCase)


if __name__ == '__main__':
    unittest.main()
