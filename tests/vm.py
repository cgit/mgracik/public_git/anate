import unittest
import anate.vm as vm


class VMTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testVirtualDisk(self):
        disk = vm.VirtualDisk('mydisk1', 10000, 100)
        self.assertEqual(disk.name, 'mydisk1')
        self.assertEqual(disk.path, '/dev/mapper/mydisk1')
        self.assertEqual(disk.size, 10000)
        self.assertEqual(disk.usable_size, 100)
        self.assertFalse(disk.active)

        disk.create()
        self.assertTrue(disk.active)
        self.assertEqual(disk._loop_dev.major, 7)
        self.assertEqual(disk._loop_dev.minor, 0)

        disk.remove()
        self.assertFalse(disk.active)

    def testVM(self):
        machine = vm.VM()

        machine.addDisk('mydisk1', 10000, 100)
        machine.addDisk('mydisk2', 10000, 100)
        self.assertEqual(len(machine.disks), 2)

        for name in ('mydisk1', 'mydisk2'):
            disk = machine.getDisk(name)
            self.assertEqual(disk.name, name)
            self.assertEqual(disk.path, '/dev/mapper/' + name)
            self.assertEqual(disk.size, 10000)
            self.assertEqual(disk.usable_size, 100)
            self.assertTrue(disk.active)

        machine.removeDisk('mydisk2')
        machine.removeDisk('mydisk1')
        self.assertEqual(machine.disks, {})

        machine.halt()


def suite():
    return unittest.TestLoader().loadTestsFromTestCase(VMTestCase)


if __name__ == '__main__':
    unittest.main()
