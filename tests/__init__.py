import os

def getAvailableSuites():
    root, tests_dir = os.path.split(os.path.dirname(__file__))
    modules = []

    for root, dirs, files in os.walk(tests_dir):
        for filename in files:
            if filename.endswith('.py') and filename != '__init__.py':
                basename, extension = os.path.splitext(filename)
                modules.append(os.path.join(root, basename).replace('/', '.'))

    available_suites = {}
    for module in modules:
        imported = __import__(module, globals(), locals(), [module], -1)
        suite = getattr(imported, 'suite', None)
        if callable(suite):
            available_suites[module] = suite()

    return available_suites
