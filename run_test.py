#!/usr/bin/env python

import sys
import unittest
import tests
import string
from optparse import OptionParser


def getFullSuiteName(suite, full_suite_names):
    suites = []
    for full_suite_name in full_suite_names:
        if full_suite_name.lower().find(suite) != -1:
            suites.append(full_suite_name)

    return suites


def main():
    usage = 'Usage: %prog [options] [arg1 arg2 ... argN]'
    version = '%prog 0.1'
    parser = OptionParser(usage=usage, version=version)
    parser.add_option('-l', '--list', action='store_true', default=False,
                      help='print all available test suites and exit')
    parser.add_option('-i', '--interactive', action='store_true', default=False,
                      help='run in the interactive mode')

    (options, args) = parser.parse_args(sys.argv[1:])
    if not args and not (options.list or options.interactive):
        parser.error('you must specify either an option or an argument\n'
                     'Try "%s --help" for more information' % sys.argv[0])

    print 'Searching for test suites'
    available_suites = tests.getAvailableSuites()
    if not available_suites:
        print 'No test suites available, exiting'
        sys.exit(1)

    suite_names = available_suites.keys()
    suite_names.sort()

    if options.list and not options.interactive:
        print '\nAvailable test suites:'
        for suite_name in suite_names:
            print suite_name
        sys.exit(0)

    suites_to_run = []
    if options.interactive:
        print '\nAvailable test suites:'
        suite_num = 0
        for suite_name in suite_names:
            print '[%3d] %s' % (suite_num, suite_name)
            suite_num += 1

        try:
            input_string = raw_input('\nType in the test you want to run, '
                                     'or "all" to run all listed tests: ')
        except KeyboardInterrupt:
            print '\nAborted by user'
            sys.exit(1)

        for arg in input_string.split():
            if arg.isdigit():
                arg = int(arg)
                try:
                    args.append(suite_names[arg])
                except KeyError:
                    pass
            else:
                args.append(arg)

    args = map(string.lower, args)
    if 'all' in args:
        suites_to_run = suite_names[:]
    else:
        for arg in args:
            matching_suites = getFullSuiteName(arg, suite_names)
            suites_to_run.extend(filter(lambda suite: suite not in suites_to_run,
                                        matching_suites))

    if suites_to_run:
        suites_to_run.sort()
        print 'Running test suites: %s' % suites_to_run
        suite = unittest.TestSuite([available_suites[suite_name]
                                    for suite_name in suites_to_run])

        try:
            result = unittest.TextTestRunner(verbosity=2).run(suite)
        except KeyboardInterrupt:
            print '\nAborted by user'
            sys.exit(1)

        if result.wasSuccessful():
            print '\nAll test suites OK'
            sys.exit(0)
        else:
            print '\nTest suites finished with %d errors and %d failures' % (len(result.errors),
                                                                             len(result.failures))
            sys.exit(2)
    else:
        print 'No test suites matching your criteria found, exiting'
        sys.exit(1)


if __name__ == '__main__':
    main()
